import { Movie, MovieFormatted } from "../hooks/useFetchMovies";

export const convertKeysToCamelCase = (movie: Movie): MovieFormatted => {
    return {
        id: movie.id,
        title: movie.title,
        posterPath: movie.poster_path,
        releaseDate: movie.release_date,
        voteAverage: movie.vote_average,
        overview: movie.overview,
    };
};
