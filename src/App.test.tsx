import { convertKeysToCamelCase } from "./utils/convertKeysToCamelCase";

test("convertKeysToCamelCase", () => {
    const movie = {
        id: 1,
        title: "Movie Title",
        poster_path: "/path/",
        release_date: "2023-07-19",
        vote_average: 7.5,
        overview: "Movie overview",
    };

    const expectedMovie = {
        id: 1,
        title: "Movie Title",
        posterPath: "/path/",
        releaseDate: "2023-07-19",
        voteAverage: 7.5,
        overview: "Movie overview",
    };

    const convertedMovie = convertKeysToCamelCase(movie);

    expect(convertedMovie).toEqual(expectedMovie);
});
