import React from "react";
import "../App.css";
import useFetchMovies from "../hooks/useFetchMovies";
import { MovieCard } from "../components/movieCard";

export const Home = () => {
    const moviesDetails = useFetchMovies();

    return (
        <>
            <h1>Movies</h1>
            <div className="movies-container">
                {moviesDetails?.map((movieDetails) => {
                    return (
                        <MovieCard
                            overview={movieDetails.overview}
                            key={movieDetails.id}
                            id={movieDetails.id}
                            posterPath={movieDetails.posterPath}
                            releaseDate={movieDetails.releaseDate}
                            title={movieDetails.title}
                            voteAverage={movieDetails.voteAverage}
                        />
                    );
                })}
            </div>
        </>
    );
};
