import axios from "axios";
import { useEffect, useState } from "react";
import { convertKeysToCamelCase } from "../utils/convertKeysToCamelCase";

export type Movie = {
    id: number;
    title: string;
    poster_path: string;
    release_date: string;
    vote_average: number;
    overview: string;
};

export type MovieFormatted = {
    id: number;
    title: string;
    posterPath: string;
    releaseDate: string;
    voteAverage: number;
    overview: string;
};

const useFetchMovies = () => {
    const [moviesDetails, setMoviesDetails] = useState<MovieFormatted[]>([]);

    // should be in .env
    const apiKey = "207e382425159ea1cfde531f73cf8170";
    const apiUrl = "https://api.themoviedb.org/3/discover/movie/?api_key=";

    const fetchMovies = async () => {
        const response = await axios.get(`${apiUrl}${apiKey}`);
        const formattedMovies = response.data.results.map((movie: Movie) => convertKeysToCamelCase(movie));
        setMoviesDetails(formattedMovies);
    };
    useEffect(() => {
        fetchMovies();
    }, []);

    return moviesDetails;
};

export default useFetchMovies;
