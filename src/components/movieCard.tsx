/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import Modal from "react-modal";
import React, { useState } from "react";
import { MovieFormatted } from "../hooks/useFetchMovies";
import "../App.css";
import { modalStyles } from "../styles/ModalStyle";

export const MovieCard = (movieDetail: MovieFormatted) => {
    const { id, title, posterPath, releaseDate, voteAverage, overview } = movieDetail;

    const getFormattedVote = (vote: number) => {
        return `${vote} / 10`;
    };

    const [modalIsOpen, setModalIsOpen] = useState(false);

    const onMovieClick = (modalIsOpen: boolean) => {
        setModalIsOpen(modalIsOpen);
    };

    const closeModal = () => {
        setModalIsOpen(false);
    };

    Modal.setAppElement("#root");

    return (
        <>
            <div className="movie-card" key={id} onClick={() => onMovieClick(!modalIsOpen)}>
                <p>{title}</p>
                <p>{getFormattedVote(voteAverage)}</p>
                <img src={`https://image.tmdb.org/t/p/w500/${posterPath}`} height={150} width={100} alt="film" />
                <p>{releaseDate}</p>
            </div>
            <Modal isOpen={modalIsOpen} style={modalStyles} contentLabel="Example Modal">
                <h2>Overview of {title}</h2>
                <p>{overview}</p>
                <button type="button" onClick={closeModal}>
                    X
                </button>
            </Modal>
        </>
    );
};
